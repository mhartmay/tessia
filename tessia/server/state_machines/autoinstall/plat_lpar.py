# Copyright 2016, 2017 IBM Corp.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Module to deal with operations on LPARs
"""

#
# IMPORTS
#
from copy import deepcopy
from tessia.server.config import Config
from tessia.server.state_machines.autoinstall.plat_base import PlatBase
from urllib.parse import urljoin

import logging

#
# CONSTANTS AND DEFINITIONS
#

#
# CODE
#
class PlatLpar(PlatBase):
    """
    Handling for HMC's LPARs
    """

    def __init__(self, *args, **kwargs):
        """
        Constructor, validate values provided.
        """
        super().__init__(*args, **kwargs)
        # create our own logger so that the right module name is in output
        self._logger = logging.getLogger(__name__)

        # make sure the CPC of the LPAR has a live-image disk configured
        try:
            self._live_disk = self._hyp_prof.storage_volumes_rel[0]
        except IndexError:
            raise ValueError(
                'CPC {} has no disk configured to serve live-image'
                .format(self._hyp_prof.system_rel.name)) from None
        config = Config.get_config()
        try:
            self._live_passwd = config['auto_install']['live_img_passwd']
        except KeyError:
            raise ValueError(
                'Live-image password missing in config file') from None
    # __init__()

    def boot(self, kargs):
        """
        Perform a boot operation so that the installation process can start.

        Args:
            kargs (str): kernel command line args for os' installer
        """
        # basic information
        cpu = self._guest_prof.cpu
        memory = self._guest_prof.memory
        guest_name = self._guest_prof.system_rel.name

        # repository related information
        repo = self._repo
        kernel_uri = urljoin(repo.url + '/', repo.kernel.strip('/'))
        initrd_uri = urljoin(repo.url + '/', repo.initrd.strip('/'))

        # parameters argument, see baselib schema for details
        params = {}
        if self._live_disk.type.lower() == 'fcp':
            params['boot_params'] = {
                'boot_method': 'scsi',
                'zfcp_devicenr': self._live_disk.specs['adapters'][0]['devno'],
                'wwpn': self._live_disk.specs['adapters'][0]['wwpns'][0],
                'lun': self._live_disk.volume_id
            }
        else:
            params['boot_params'] = {
                'boot_method': 'dasd',
                'devicenr': self._live_disk.volume_id
            }
        params['boot_params']['netboot'] = {
            "kernel_url": kernel_uri,
            "initrd_url": initrd_uri,
            "cmdline": kargs
        }
        # network configuration
        options = deepcopy(self._gw_iface['attributes'])
        options.pop('ccwgroup')
        params['boot_params']['netsetup'] = {
            "mac": self._gw_iface['mac_addr'],
            "ip": self._gw_iface['ip'],
            "mask": self._gw_iface["mask"],
            "gateway": self._gw_iface['gateway'],
            "device": self._gw_iface['attributes']
                      ['ccwgroup'].split(",")[0].split('.')[-1],
            "options": options,
            "password": self._live_passwd,
        }
        dns_servers = []
        if self._gw_iface.get('dns_1'):
            dns_servers.append(self._gw_iface['dns_1'])
        if self._gw_iface.get('dns_2'):
            dns_servers.append(self._gw_iface['dns_2'])
        if dns_servers:
            params['boot_params']['netsetup']['dns'] = dns_servers

        self._hyp_obj.start(guest_name, cpu, memory, params)
    # boot()

    def get_vol_devpath(self, vol_obj):
        """
        Given a volume entry, return the correspondent device path on operating
        system.
        """
        if vol_obj.type == 'DASD':
            vol_id = vol_obj.volume_id
            if vol_id.find('.') < 0:
                vol_id = '0.0.' + vol_id
            return '/dev/disk/by-path/ccw-{}'.format(vol_id)

        elif vol_obj.type == 'FCP':
            if vol_obj.specs['multipath']:
                prefix = '/dev/disk/by-id/dm-uuid-mpath-{}'
            else:
                prefix = '/dev/disk/by-id/scsi-{}'
            return prefix.format(vol_obj.specs['wwid'])

        raise RuntimeError(
            "Unknown volume type'{}'".format(vol_obj.type))

    # get_vol_devpath()
# PlatLpar
